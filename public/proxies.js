// eslint-disable-next-line
(function () {
    class ProxyHandlerClass {
        constructor() {
            this.proxies = [
                //
            ]
            this.proxiedUrls = [
                //
            ];
            this.idxCurrentProxy = 0;
            this.useDevServer = false;
            this.useStaticProxy = false; // Set to true to replace all URLs in incoming data
            this.setRandomProxy(); // Init to random
        }

        getCurrentProxy = function () {
            if (this.useDevServer == true) {
                // For development, use the development proxy set up in vue.config.js
                return '/rfa';
            }
            return this.proxies[this.idxCurrentProxy];
        }

        setCurrentProxy = function (proxy) {
            if (proxy) {
                this.idxCurrentProxy = this.proxies.indexOf(proxy);
                if (this.idxCurrentProxy >= 0) {
                    return; // success
                }
            }

            // Not set or invlid, use a random one!
            this.setRandomProxy();
        }

        setRandomProxy() {
            this.idxCurrentProxy = Math.floor(Math.random() * Math.floor(this.proxies.length));
            console.log("RANDOM PROXY ----> " + this.getCurrentProxy());
        }

        moveToNextProxy = function () {
            this.idxCurrentProxy = (this.idxCurrentProxy + 1) % this.proxies.length;
            console.log("NEXT PROXY ----> " + this.getCurrentProxy());
            return this.getCurrentProxy();
        }

        ensureTrailingSlash(url) {
            if (url.endsWith("/")) {
                return url;
            }
            return url + "/";
        }

        getProxiedUrl(url) {
            if (url) {
                for (var p of this.proxiedUrls) {
                    const proxiedUrl = this.ensureTrailingSlash(p);
                    if (url.startsWith(proxiedUrl)) {
                        const proxy = this.getCurrentProxy();
                        var path = url.split("://")[1];
                        path = path.substring(0, path.indexOf("/") + 1);
                        const urlRewritten = url.replace(proxiedUrl, proxy + "/" + path);
                        console.log("getProxiedUrl rewrite: " + url + " -> " + urlRewritten);
                        return urlRewritten;
                    }
                }
            }
            return url;
        }
    }
    this.ProxyHandler = new ProxyHandlerClass();
}());
