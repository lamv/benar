import Vue from "vue"
import Vuex from "vuex"
import VuexPersist from "vuex-persist"

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
	key: "settings",
	storage: localStorage,
	reducer: (state) => ({
		onboarded: state.onboarded,
		showMedia: state.showMedia,
		flavor: state.flavor,
		flavorStatic: state.flavorStatic,
		flavorsAvailable: state.flavorsAvailable,
		textSizeAdjustment: state.textSizeAdjustment,
		currentAppVersion: state.currentAppVersion,
		currentProxy: state.currentProxy,
	}),
})

export default new Vuex.Store({
	state: {
		onboarded: false,
		showMedia: false,
		flavor: "unknown",
		flavorStatic: false,
		flavorsAvailable: null,
		currentAppVersion: null, // Last known version we ran. Used for displaying "you just updated to ..."
		textSizeAdjustment: 0,
		currentFeedTitle: "",
		currentFeedItems: [],
		currentFeedCategories: [],
		currentFeedCategoriesWithItems: [],
		fullScreenItems: null,
		fullScreenItemIndex: -1,
		isLandscapeMode: false,
		showingFullScreenVideo: false,
		currentProxy: null,
		isAudioPlaying: false,
		mediaPlayerItem: [],
	},
	mutations: {
		onboarded(state, onboarded) {
			state.onboarded = onboarded
		},
		showMedia(state, value) {
			state.showMedia = value
		},
		setFlavor(state, flavor) {
			state.flavor = flavor
		},
		setFlavorStatic(state, flavorStatic) {
			state.flavorStatic = flavorStatic
		},
		setFlavorsAvailable(state, flavorsAvailable) {
			state.flavorsAvailable = flavorsAvailable
		},
		setCurrentAppVersion(state, version) {
			state.currentAppVersion = version
		},
		setTextSizeAdjustment(state, adjustment) {
			state.textSizeAdjustment = adjustment
		},
		setCurrentFeedTitle(state, title) {
			state.currentFeedTitle = title
		},
		setCurrentFeedItems(state, items) {
			state.currentFeedItems = items
		},
		setCurrentVideoFeedItems(state, items) {
			state.currentVideoFeedItems = items
		},
		clearCategories(state, numCategories) {
			state.currentFeedCategories = []
			state.currentFeedCategoriesWithItems = []
			state.currentVideoFeedItems = []
			for (var i = 0; i < numCategories; i++) {
				state.currentFeedCategories.push({feed: null, items: null})
			}
		},
		addCategoryItems(state, category) {
			if (typeof category !== 'undefined') {
					Object.assign(state.currentFeedCategories[category.index], category);
					//use withItems feed instead of feedCat
					state.currentFeedCategoriesWithItems = [];
					for (var i = 0; i < state.currentFeedCategories.length; i++) {
							if (state.currentFeedCategories[i].items != null) {
									state.currentFeedCategoriesWithItems.push(state.currentFeedCategories[i]);
							}
					}
			}
	},
	
		addVideoItems(state, videoItems) {
			state.currentVideoFeedItems = videoItems
		},
		
		setFullScreenItems(state, data) {
			state.fullScreenItems = data.items
			state.fullScreenItemIndex = data.activeIndex
		},
		setFullScreenItemIndex(state, index) {
			state.fullScreenItemIndex = index
		},
		setLandscapeMode(state, isLandscapeMode) {
			state.isLandscapeMode = isLandscapeMode
		},
		showingFullScreenVideo(state, showingFullScreenVideo) {
			state.showingFullScreenVideo = showingFullScreenVideo
		},
		setCurrentProxy(state, proxy) {
			state.currentProxy = proxy
		},
		setIsAudioPlaying(state, status) {
			state.isAudioPlaying = status
		},
		setMediaPlayerItem(state, item) {
			state.mediaPlayerItem = item
		},
	},
	plugins: [vuexPersist.plugin],
})
